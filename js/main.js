let priceForm = document.getElementById('priceForm');
let container = document.getElementById('container');
let span = document.createElement('span');
span.setAttribute('class','result');
container.appendChild(span);
let close = document.createElement('a');
close.setAttribute('class','cross');
container.appendChild(close);
let warning = document.createElement('p');
document.body.appendChild(warning);
priceForm.onfocus = function () {
  priceForm.classList.add('active');
};
priceForm.onblur = function () {
    if (priceForm.value > 0) {
        priceForm.classList.remove('active');
        priceForm.classList.remove('error');
        container.classList.add('active-container');
        warning.innerText = '';
        span.innerHTML = `Present value: ${priceForm.value}`;
        close.innerHTML = 'X';
    }
    else if (priceForm.value < 0 || priceForm.value === ''){
        priceForm.classList.toggle('error');
        container.classList.remove('active-container');
warning.innerText = 'Please enter correct price'
    }
};
close.onclick = function () {
    container.classList.remove('active-container');
    priceForm.value = '';
};

// close.addEventListener('click',closeCross);
// function closeCross() {
// container.classList.remove('active-container')
// }